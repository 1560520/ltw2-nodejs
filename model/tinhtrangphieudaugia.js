var connection = require("./connectionString");
const Client = require("pg").Client;

module.exports = class TinhTrangPhieuDauGia {
  constructor(matinhtrangphieudg = null, tentinhtrangphieudg = null) {
    this.matinhtrangphieudg = matinhtrangphieudg;
    this.tentinhtrangphieudg = tentinhtrangphieudg;
  }

  async create(tinhtrangphieudaugia) {
    const client = new Client(connection);
    console.log("Class TinhTrangPhieuDauGia: create");
    await client.connect();
    try {
      const res = await client.query(
        "insert into tinhtrangphieudaugia(tentinhtrangphieudg) values($1) returning *",
        [tinhtrangphieudaugia.tentinhtrangphieudg]
      );
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }

  async read() {
    const client = new Client(connection);
    console.log("Class TinhTrangPhienDauGia: read");
    await client.connect();
    try {
      const res = await client.query("SELECT * from tinhtrangphieudaugia order by matinhtrangphieudg asc");
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }

  async update(tinhtrangphieudaugia) {
    const client = new Client(connection);
    console.log("Class TinhTrangPhieuDauGia: update");
    await client.connect();
    try {
      const res = await client.query(
        "update tinhtrangphieudaugia set tentinhtrangphieudg = $1 where matinhtrangphieudg = $2 returning *",
        [
          tinhtrangphieudaugia.tentinhtrangphieudg,
          tinhtrangphieudaugia.matinhtrangphieudg,
        ]
      );
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }

  async delete(id) {
    const client = new Client(connection);
    console.log("Class TinhTrangPhieuDauGia: delete");
    await client.connect();
    try {
      const res = await client.query(
        "delete from tinhtrangphieudaugia where matinhtrangphieudg = $1 returning *",
        [id]
      );
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }

  async get(id) {
    const client = new Client(connection);
    console.log("Class TinhTrangPhieuDauGia: get");
    await client.connect();
    try {
      const res = await client.query(
        "select * from tinhtrangphieudaugia where matinhtrangphieudg = $1",
        [id]
      );
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }
};
