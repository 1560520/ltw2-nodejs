var connection = require("./connectionString");
const Client = require("pg").Client;

module.exports = class SanPham {
  constructor(masp = null, tensp = null, maloaisp = null, dacta = null, hinhdaidien = null, isnew = null, isdelete = null, created = null, updated = null) {
    this.masp = masp;
    this.tensp = tensp;
    this.maloaisp = maloaisp;
    this.dacta = dacta;
    this.hinhdaidien = hinhdaidien;
    this.isnew = isnew;
    this.isdelete = isdelete;
    this.created = created;
    this.updated = updated;
  }
  async create(sanpham) {
    const client = new Client(connection);
    await client.connect();
    try {
      const res = await client.query(
        "insert into sanpham(tensp,maloaisp,dacta,hinhdaidien,isnew,isdelete,created,updated) values($1,$2,$3,$4,$5,$6,$7,$8) returning *",
        [sanpham.tensp, sanpham.maloaisp, sanpham.dacta, sanpham.hinhdaidien, sanpham.isnew, sanpham.isdelete, sanpham.created, sanpham.updated]
      );
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }
  async read() {
    const client = new Client(connection);
    console.log("Class sanpham: read");
    await client.connect();
    try {
      const res = await client.query("SELECT * from sanpham where isdelete = false order by masp asc");
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }

  async update(sanpham) {
    const client = new Client(connection);
    console.log("Class sanpham: update");
    await client.connect();
    try {
      const res = await client.query(
        "update sanpham set tensp = $1, maloaisp = $2, dacta = $3, hinhdaidien = $4, isnew = $5, isdelete = $6, created = $7, updated = $8 where masp = $9 returning *",
        [sanpham.tensp, sanpham.maloaisp, sanpham.dacta, sanpham.hinhdaidien, sanpham.isnew, sanpham.isdelete, sanpham.created, sanpham.updated, sanpham.masp]
      );
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }

  async delete(id) {

    const client = new Client(connection);
    console.log("Class sanpham: delete");
    await client.connect();
    try {
      const res = await client.query(
        "update sanpham set isdelete = true where masp = $1 returning *",
        [id]
      );
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }

  async get(id) {
    const client = new Client(connection);
    console.log("Class sanpham: get");
    await client.connect();
    try {
      const res = await client.query("select * from sanpham where masp = $1", [id]);
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }
};