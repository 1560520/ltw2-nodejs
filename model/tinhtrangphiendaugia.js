var connection = require("./connectionString");
const Client = require("pg").Client;

module.exports = class TinhTrangPhienDauGia {
    constructor(matinhtrangphiendg = null, tentinhtrangphiendg = null) {
        this.matinhtrangphiendg = matinhtrangphiendg;
        this.tentinhtrangphiendg = tentinhtrangphiendg;
    }

    async create(tinhtrangphiendaugia) {
        const client = new Client(connection);
        console.log("Class TinhTrangPhienDauGia: create");
        await client.connect();
        try {
            const res = await client.query(
                "insert into tinhtrangphiendaugia(tentinhtrangphiendg) values($1) returning *", [tinhtrangphiendaugia.tentinhtrangphiendg]
            );
            return res.rows;
        } catch (err) {
            console.log(err.stack);
        }
        client.end();
    }

    async read() {
        const client = new Client(connection);
        console.log("Class TinhTrangPhienDauGia: read");
        await client.connect();
        try {
            const res = await client.query("SELECT * from tinhtrangphiendaugia order by matinhtrangphiendg asc");
            return res.rows;
        } catch (err) {
            console.log(err.stack);
        }
        client.end();
    }

    async update(tinhtrangphiendaugia) {
        const client = new Client(connection);
        console.log("Class TinhTrangPhienDauGia: update");
        await client.connect();
        try {
            const res = await client.query(
                "update tinhtrangphiendaugia set tentinhtrangphiendg = $1 where matinhtrangphiendg = $2 returning *", [
                    tinhtrangphiendaugia.tentinhtrangphiendg,
                    tinhtrangphiendaugia.matinhtrangphiendg,
                ]
            );
            return res.rows;
        } catch (err) {
            console.log(err.stack);
        }
        client.end();
    }

    async delete(id) {
        const client = new Client(connection);
        console.log("Class TinhTrangPhienDauGia: delete");
        await client.connect();
        try {
            const res = await client.query(
                "delete from tinhtrangphiendaugia where matinhtrangphiendg = $1 returning *", [id]
            );
            return res.rows;
        } catch (err) {
            console.log(err.stack);
        }
        client.end();
    }
    async get(id) {
        const client = new Client(connection);
        console.log("Class TinhTrangPhienDauGia: get");
        await client.connect();
        try {
            const res = await client.query(
                "select * from tinhtrangphiendaugia where matinhtrangphiendg = $1", [id]
            );
            return res.rows;
        } catch (err) {
            console.log(err.stack);
        }
        client.end();
    }
};