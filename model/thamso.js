var connection = require("./connectionString");
const Client = require("pg").Client;

module.exports = class ThamSo {
  constructor(mathamso = null, tenthamso = null, giatri = null) {
    this.mathamso = mathamso;
    this.tenthamso = tenthamso;
    this.giatri = giatri;
  }
  async create(thamso) {
    const client = new Client(connection);
    console.log("Class ThamSo: create");
    await client.connect();
    try {
      const res = await client.query(
        "insert into thamso(tenthamso,giatri) values($1,$2) returning *",
        [thamso.tenthamso, thamso.giatri]
      );
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }
  async read() {
    const client = new Client(connection);
    console.log("Class ThamSo: read");
    await client.connect();
    try {
      const res = await client.query("SELECT * from thamso order by mathamso asc");
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }

  async update(thamso) {
    const client = new Client(connection);
    console.log("Class ThamSo: update");
    await client.connect();
    try {
      const res = await client.query(
        "update thamso set giatri = $1 where mathamso = $2 returning *",
        [thamso.giatri, thamso.mathamso]
      );
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }

  async delete(id) {
    const client = new Client(connection);
    console.log("Class ThamSo: delete");
    await client.connect();
    try {
      const res = await client.query(
        "delete from thamso where mathamso = $1 returning *",
        [id]
      );
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }

  async get(id) {
    const client = new Client(connection);
    console.log("Class ThamSo: get");
    await client.connect();
    try {
      const res = await client.query(
        "select * from thamso where mathamso = $1",
        [id]
      );
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }
};
