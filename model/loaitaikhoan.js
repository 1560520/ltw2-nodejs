var connection = require("./connectionString");
const Client = require("pg").Client;

module.exports = class LoaiTaiKhoan {
  constructor(maloaitk = null, tenloaitk = null) {
    this.maloaitk = maloaitk;
    this.tenloaitk = tenloaitk;
  }

  async create(loaitaikhoan) {
    const client = new Client(connection);
    console.log("Class LoaiTaiKhoan: create");
    await client.connect();
    try {
      const res = await client.query(
        "insert into loaitaikhoan(tenloaitk) values($1) returning *",
        [loaitaikhoan.tenloaitk]
      );
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }
  async read() {
    const client = new Client(connection);
    console.log("Class LoaiTaiKhoan: read");
    await client.connect();
    try {
      const res = await client.query("SELECT * from loaitaikhoan order by maloaitk asc");
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }

  async update(loaitaikhoan) {
    const client = new Client(connection);
    console.log("Class LoaiTaiKhoan: update");
    await client.connect();
    try {
      const res = await client.query(
        "update loaitaikhoan set tenloaitk = $1 where maloaitk = $2 returning *",
        [loaitaikhoan.tenloaitk, loaitaikhoan.maloaitk]
      );
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }

  async delete(id) {
    const client = new Client(connection);
    console.log("Class LoaiTaiKhoan: delete");
    await client.connect();
    try {
      const res = await client.query(
        "delete from loaitaikhoan where maloaitk = $1 returning *", [id]
      );
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }

  async get(id) {
    const client = new Client(connection);
    console.log("Class LoaiTaiKhoan: get");
    await client.connect();
    try {
      const res = await client.query("select * from loaitaikhoan where maloaitk = $1", [id]
      );
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }
};

