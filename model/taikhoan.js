var connection = require("./connectionString");
const Client = require("pg").Client;

module.exports = class TaiKhoan {
  constructor(
    matk = null,
    tendangnhap = null,
    matkhau = null,
    tenhienthi = null,
    email = null,
    dienthoai = null,
    diachi = null,
    maloaitk = null
  ) {
    this.matk = matk;
    this.tendangnhap = tendangnhap;
    this.matkhau = matkhau;
    this.tenhienthi = tenhienthi;
    this.email = email;
    this.dienthoai = dienthoai;
    this.diachi = diachi;
    this.maloaitk = maloaitk;
  }
  async create(taikhoan) {
    const client = new Client(connection);
    console.log("Class TaiKhoan: create");
    await client.connect();
    try {
      const res = await client.query(
        "insert into taikhoan(tendangnhap,matkhau,tenhienthi,email,dienthoai,diachi,maloaitk) values($1,$2,$3,$4,$5,$6,$7) returning *",
        [
          taikhoan.tendangnhap,
          taikhoan.matkhau,
          taikhoan.tenhienthi,
          taikhoan.email,
          taikhoan.dienthoai,
          taikhoan.diachi,
          taikhoan.maloaitk,
        ]
      );
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }
  async read() {
    const client = new Client(connection);
    console.log("Class TaiKhoan: read");
    await client.connect();
    try {
      const res = await client.query("SELECT * from taikhoan order by matk asc");
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }

  async update(taikhoan) {
    const client = new Client(connection);
    console.log("Class TaiKhoan: update");
    await client.connect();
    try {
      const res = await client.query(
        "update taikhoan set tendangnhap = $1, matkhau=$2,tenhienthi=$3,email=$4,dienthoai=$5, diachi=$6, maloaitk=$7 where matk = $8 returning *",
        [
          taikhoan.tendangnhap,
          taikhoan.matkhau,
          taikhoan.tenhienthi,
          taikhoan.email,
          taikhoan.dienthoai,
          taikhoan.diachi,
          taikhoan.maloaitk,
          taikhoan.matk,
        ]
      );
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }

  async delete(id) {
    const client = new Client(connection);
    console.log("Class TaiKhoan: delete");
    await client.connect();
    try {
      const res = await client.query(
        "delete from taikhoan where matk = $1 returning *",
        [id]
      );
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }
  async get(tendangnhap, matkhau) {
    const client = new Client(connection);
    console.log("Class TaiKhoan: get");
    await client.connect();
    try {
      const res = await client.query(
        "select * from taikhoan where tendangnhap = $1 and matkhau = $2 and maloaitk != 3",
        [tendangnhap, matkhau]
      );
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }

  async checkUsername(tendangnhap) {
    const client = new Client(connection);
    console.log("Class TaiKhoan: checkUsername");
    await client.connect();
    try {
      const res = await client.query(
        "select * from taikhoan where tendangnhap = $1",
        [tendangnhap]
      );
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }
  async getUser(matk) {
    const client = new Client(connection);
    console.log("Class TaiKhoan: getUser");
    await client.connect();
    try {
      const res = await client.query(
        "select * from taikhoan where matk = $1",
        [matk]
      );
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }
};
