var connection = require("./connectionString");
const Client = require("pg").Client;

module.exports = class Hinh {
  constructor(mahinh = null, duongdan = null, masp = null) {
    this.mahinh = mahinh;
    this.duongdan = duongdan;
    this.masp = masp;
  }
  async create(hinh) {
    const client = new Client(connection);
    console.log("Class hinh: create");
    await client.connect();
    try {
      const res = await client.query(
        "insert into hinh(duongdan,masp) values($1,$2) returning *",
        [hinh.duongdan, hinh.masp]
      );
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }
  async read() {
    const client = new Client(connection);
    console.log("Class hinh: read");
    await client.connect();
    try {
      const res = await client.query("SELECT * from hinh order by mahinh asc");
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }

  async update(hinh) {
    const client = new Client(connection);
    console.log("Class hinh: update");
    await client.connect();
    try {
      const res = await client.query(
        "update hinh set masp = $1 where mahinh = $2 returning *",
        [hinh.masp, hinh.mahinh]
      );
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }

  async delete(id) {
    const client = new Client(connection);
    console.log("Class hinh: delete");
    await client.connect();
    try {
      const res = await client.query(
        "delete from hinh where mahinh = $1 returning *",
        [id]
      );
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }

  async get(id) {
    const client = new Client(connection);
    console.log("Class hinh: get");
    await client.connect();
    try {
      const res = await client.query("select * from hinh where mahinh = $1", [id]);
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }
};
