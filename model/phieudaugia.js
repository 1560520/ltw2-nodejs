var connection = require("./connectionString");
const Client = require("pg").Client;

module.exports = class PhieuDauGia {
    constructor(maphieudg = null, maphiendg = null, matk = null, giadau = null, tinhtrangphieudg = null, thoigiandau = null) {
        this.maphieudg = maphieudg;
        this.maphiendg = maphiendg;
        this.matk = matk;
        this.giadau = giadau;
        this.tinhtrangphieudg = tinhtrangphieudg;
        this.thoigiandau = thoigiandau;
    }
    async create(phieudaugia) {
        const client = new Client(connection);
        console.log("Class PhieuDauGia: create");
        await client.connect();
        try {
            const res = await client.query(
                "insert into phieudaugia(maphiendg,matk,giadau,tinhtrangphieudg,thoigiandau) values($1,$2,$3,$4,$5) returning *", [phieudaugia.maphiendg, phieudaugia.matk, phieudaugia.giadau, phieudaugia.tinhtrangphieudg, phieudaugia.thoigiandau]
            );
            return res.rows;
        } catch (err) {
            console.log(err.stack);
        }
        client.end();
    }
    async read() {
        const client = new Client(connection);
        console.log("Class PhieuDauGia: read");
        await client.connect();
        try {
            const res = await client.query("SELECT * from phieudaugia order by maphieudg asc");
            return res.rows;
        } catch (err) {
            console.log(err.stack);
        }
        client.end();
    }


    async update(phieudaugia) {
        const client = new Client(connection);
        console.log("Class PhieuDauGia: update");
        await client.connect();
        try {
            const res = await client.query(
                "update phieudaugia set maphiendg = $1,matk=$2,giadau=$3,tinhtrangphieudg=$4,thoigiandau=$6 where maphieudg = $5 returning *", [phieudaugia.maphiendg, phieudaugia.matk, phieudaugia.giadau, phieudaugia.tinhtrangphieudg, phieudaugia.thoigiandau]
            );
            return res.rows;
        } catch (err) {
            console.log(err.stack);
        }
        client.end();
    }

    async updatetrangthaiphieudgthua(maphiendg) {
        const client = new Client(connection);
        console.log("Class PhieuDauGia: updatetrangthaithua");
        await client.connect();
        try {
            const res = await client.query(
                "update phieudaugia set tinhtrangphieudg = 2 where maphiendg = $1 ", [maphiendg]
            );
            return res.rows;
        } catch (err) {
            console.log(err.stack);
        }
        client.end();
    }

    async updatetrangthaiphieudgthang(maphiendg) {
        const client = new Client(connection);
        console.log("Class PhieuDauGia: updatetrangthaiphieudgthang");
        await client.connect();
        try { 
            const res = await client.query(
                "update phieudaugia set tinhtrangphieudg = 1 where maphiendg = $1 and maphieudg=(select maphieudg from phiendaugia where maphiendg=$1)", [maphiendg]
            );
            return res.rows;
        } catch (err) {
            console.log(err.stack);
        }
        client.end();
    }

    async delete(id) {
        const client = new Client(connection);
        console.log("Class PhieuDauGia: delete");
        await client.connect();
        try {
            const res = await client.query(
                "delete from phiendaugia where maphieudg = $1 returning *", [id]
            );
            return res.rows;
        } catch (err) {
            console.log(err.stack);
        }
        client.end();
    }

    async get(id) {
        const client = new Client(connection);
        console.log("Class PhienDauGia: get");
        await client.connect();
        try {
            const res = await client.query(
                "select * from phiendagia where maphieudg = $1", [id]
            );
            return res.rows;
        } catch (err) {
            console.log(err.stack);
        }
        client.end();
    }
    //Load thông tin từ giỏ hàng
    async readGioHang(matk) {
        const client = new Client(connection);
        console.log("Class PhieuDauGia: readGioHang");
        await client.connect();
        try {
            const res = await client.query(
                "select sp.tensp, phiendg.giahientai, phieudg.maphieudg from phieudaugia phieudg, phiendaugia phiendg, sanpham sp where phieudg.maphiendg=phiendg.maphiendg and phieudg.maphieudg=phiendg.maphieudg and sp.masp=phiendg.masp and phieudg.tinhtrangphieudg =1 and phiendg.tinhtrangphiendg =2 and phieudg.matk = $1", [matk]
            );
            return res.rows;
        } catch (err) {
            console.log(err.stack);
        }
        client.end();
    }

    //Cập nhật lại thông tin giỏ hàng khi bấm thanh toán
    async uploadGioHang() {
        const client = new Client(connection);
        console.log("Class PhieuDauGia: uploadGioHang");
        await client.connect();
        try {
            const res = await client.query(
                "update phiendaugia set tinhtrangphiendg = 3 where maphiendg in (select phiendg.maphiendg from phiendaugia phiendg,phieudaugia phieudg where phieudg.maphiendg=phiendg.maphiendg and  phieudg.maphieudg=phiendg.maphieudg and  phiendg.tinhtrangphiendg=2)"
            );
            return res.rows;
        } catch (err) {
            console.log(err.stack);
        }
        client.end();
    }

    //Cập nhật lại thông tin giỏ hàng khi bấm xóa
    async xoaGioHang(maphieudg) {
        const client = new Client(connection);
        console.log("Class PhieuDauGia: xoaGioHang");
        await client.connect();
        try {
            const res = await client.query(
                "update phiendaugia set tinhtrangphiendg = 4 where maphieudg in (select phiendg.maphieudg from phiendaugia phiendg,phieudaugia phieudg where phieudg.maphiendg=phiendg.maphiendg and  phieudg.maphieudg=phiendg.maphieudg and  phiendg.maphieudg = $1)", [maphieudg]
            );
            return res.rows;
        } catch (err) {
            console.log(err.stack);
        }
        client.end();
    }

    //Load thông tin đấu giá của tôi
    async readdaugiacuatoithang(matk) {
        const client = new Client(connection);
        console.log("Class PhieuDauGia: readdaugiacuatoithang");
        await client.connect();
        try {
            const res = await client.query(
                "select distinct(sp.tensp),phieudg.giadau,phieudg.thoigiandau,phiendg.giahientai,phieudg.giadau,phieudg.tinhtrangphieudg,phiendg.maphiendg from phiendaugia phiendg,phieudaugia phieudg,taikhoan tk, sanpham sp where sp.masp=phiendg.masp and phiendg.maphiendg=phieudg.maphiendg and phieudg.matk=$1 and phieudg.tinhtrangphieudg = 1 and phieudg.maphieudg = (select max(phieudg1.maphieudg) from phieudaugia phieudg1 where phieudg1.matk = tk.matk and phieudg1.maphiendg = phiendg.maphiendg and phieudg1.matk=$1)",[matk]
            );
            console.log(res.rows);
            return res.rows;
        } catch (err) {
            console.log(err.stack);
        }
        client.end();
    }
    async readdaugiacuatoithua(matk) {
        const client = new Client(connection);
        console.log("Class PhieuDauGia: readdaugiacuatoithua");
        await client.connect();
        try {
            const res = await client.query(
                "select distinct(sp.tensp),phieudg.giadau,phieudg.thoigiandau,phiendg.giahientai,phieudg.giadau,phieudg.tinhtrangphieudg,phiendg.maphiendg from phiendaugia phiendg,phieudaugia phieudg,taikhoan tk, sanpham sp where sp.masp=phiendg.masp and phiendg.maphiendg=phieudg.maphiendg and phieudg.matk=$1 and phieudg.tinhtrangphieudg = 2 and phieudg.maphieudg = (select max(phieudg1.maphieudg) from phieudaugia phieudg1 where phieudg1.matk = tk.matk and phieudg1.maphiendg = phiendg.maphiendg and phieudg1.matk=$1)",[matk]
            );
            console.log(res.rows);
            return res.rows;
        } catch (err) {
            console.log(err.stack);
        }
        client.end();
    }
    async readdaugiacuatoidangdau(matk) {
        const client = new Client(connection);
        console.log("Class PhieuDauGia: readdaugiacuatoidangdau");
        await client.connect();
        try {
            const res = await client.query(
                "select distinct(sp.tensp),phieudg.giadau,phieudg.thoigiandau,phiendg.giahientai,phieudg.giadau,phieudg.tinhtrangphieudg,phiendg.maphiendg from phiendaugia phiendg,phieudaugia phieudg,taikhoan tk, sanpham sp where sp.masp=phiendg.masp and phiendg.maphiendg=phieudg.maphiendg and phieudg.matk=$1 and phieudg.tinhtrangphieudg = 3 and phieudg.maphieudg = (select max(phieudg1.maphieudg) from phieudaugia phieudg1 where phieudg1.matk = tk.matk and phieudg1.maphiendg = phiendg.maphiendg and phieudg1.matk=$1)",[matk]
            );
            console.log(res.rows);
            return res.rows;
        } catch (err) {
            console.log(err.stack);
        }
        client.end();
    }
};