var connection = require("./connectionString");
var PhieuDauGia = require("./phieudaugia");
const Client = require("pg").Client;

module.exports = class PhienDauGia {
  constructor(
    maphiendg = null,
    masp = null,
    thoigianbd = null,
    thoigiandau = null,
    giathapnhat = null,
    giahientai = null,
    maphieudg = null,
    tinhtrangphiendg = null,
    isdelete = null,
    isnew = null,
    updated = null,
    created = null
  ) {
    this.maphiendg = maphiendg;
    this.masp = masp;
    this.thoigianbd = thoigianbd;
    this.thoigiandau = thoigiandau;
    this.giathapnhat = giathapnhat;
    this.giahientai = giahientai;
    this.maphieudg = maphieudg;
    this.tinhtrangphiendg = tinhtrangphiendg;
    this.isdelete = isdelete;
    this.isnew = isnew;
    this.updated = updated;
    this.created = created;
  }
  async create(phiendaugia) {
    const client = new Client(connection);
    console.log("Class PhienDauGia: create");
    await client.connect();
    try {
      const res = await client.query(
        "insert into phiendaugia(masp,thoigianbd,thoigiandau,giathapnhat,giahientai,tinhtrangphiendg,isdelete,isnew,updated,created) values($1,$2,$3,$4,$5,$6,$7,$8,$9,$10) returning *",
        [
          phiendaugia.masp,
          phiendaugia.thoigianbd,
          phiendaugia.thoigiandau,
          phiendaugia.giathapnhat,
          phiendaugia.giahientai,
          phiendaugia.tinhtrangphiendg,
          phiendaugia.isdelete,
          phiendaugia.isnew,
          phiendaugia.updated,
          phiendaugia.created,
        ]
      );
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }

  async read() {
    const client = new Client(connection);
    console.log("Class PhienDauGia: read");
    await client.connect();
    try {
      const res = await client.query(
        "SELECT * from phiendaugia where isdelete = false order by maphiendg asc"
      );
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }

  async update(phiendaugia) {
    const client = new Client(connection);
    console.log("Class PhienDauGia: update");
    await client.connect();
    try {
      const res = await client.query(
        "update phiendaugia set masp = $1,thoigianbd = $2,thoigiandau = $3,giathapnhat = $4,giahientai = $5,maphieudg =$6,tinhtrangphiendg =$7,isdelete = $8,isnew = $9,updated=$10,created=$11   where maphiendg = $12 returning *",
        [
          phiendaugia.masp,
          phiendaugia.thoigianbd,
          phiendaugia.thoigiandau,
          phiendaugia.giathapnhat,
          phiendaugia.giahientai,
          phiendaugia.maphieudg,
          phiendaugia.tinhtrangphiendg,
          phiendaugia.isdelete,
          phiendaugia.isnew,
          phiendaugia.updated,
          phiendaugia.created,
          phiendaugia.maphiendg,
        ]
      );
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }

  async delete(id) {
    const client = new Client(connection);
    console.log("Class PhienDauGia: delete");
    await client.connect();
    try {
      const res = await client.query(
        "update phiendaugia set isdelete = true where maphiendg = $1 returning *",
        [id]
      );
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }
  async get(id) {
    const client = new Client(connection);
    console.log("Class PhienDauGia: get");
    await client.connect();
    try {
      const res = await client.query(
        "select * from phiendaugia where maphiendg = $1",
        [id]
      );
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }
  //Load thông tin winner
  async readwinner(maphiendg) {
    const client = new Client(connection);
    console.log("Class PhienDauGia: readwinner ");
    await client.connect();
    try {
      const res = await client.query(
        `select distinct(tk.tendangnhap),phieudg.giadau,phieudg.thoigiandau,phiendg.giahientai
          from phiendaugia phiendg,phieudaugia phieudg,taikhoan tk, sanpham sp
          where phiendg.maphiendg=phieudg.maphiendg and phieudg.matk=tk.matk and phiendg.tinhtrangphiendg = 1 and phiendg.maphiendg= $1
          and phieudg.maphieudg = (select max(phieudg1.maphieudg) from phieudaugia phieudg1 where phieudg1.matk = tk.matk and phieudg1.maphiendg = phiendg.maphiendg  )
          order by phieudg.giadau desc 
          limit 3`,
        [maphiendg]
      );
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }

  async updatetrangthaiphiendg(maphiendg) {
    console.log(maphiendg);
    const client = new Client(connection);
    console.log("Class PhienDauGia: updatetrangthaiphiendg");
    await client.connect();
    try {
      const res = await client.query(
        "update phiendaugia set tinhtrangphiendg = 2 where maphiendg = $1 ",
        [maphiendg]
      );
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }

  async readthongtinphiendaugia(maphiendg) {
    const client = new Client(connection);
    console.log("Class PhienDauGia: readthongtinphiendaugia");
    await client.connect();
    try {
      const res = await client.query(
        `select phiendg.thoigianbd,phiendg.giahientai,sp.tensp,sp.dacta,sp.hinhdaidien,phiendg.thoigiandau,phiendg.thoigianbd,phiendg.masp,phiendg.giathapnhat,phiendg.tinhtrangphiendg,phiendg.isdelete,phiendg.isnew,phiendg.created,
          ROUND((extract(epoch from phiendg.thoigianbd) * 1000 + phiendg.thoigiandau * 60 * 1000 - extract(epoch from now()) * 1000) / 1000) as thoigianconlai 
          from phiendaugia phiendg, sanpham sp
          where phiendg.masp=sp.masp and phiendg.tinhtrangphiendg = 1 and phiendg.maphiendg= $1
          `,
        [maphiendg]
      );
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }

  async hometopnew() {
    const client = new Client(connection);
    console.log("Class Hometopnew: get");
    await client.connect();
    try {
      const res = await client.query(`select * , ROUND((extract(epoch from p.thoigianbd) * 1000 + p.thoigiandau * 60 * 1000 - extract(epoch from now()) * 1000) / 1000) as thoigianconlai  
          from phiendaugia p, sanpham s , loaisanpham l where s.maloaisp = l.maloaisp and p.isdelete = false and p.tinhtrangphiendg = 1 and p.masp = s.masp
          and (extract(epoch from now()) - extract(epoch from p.thoigianbd)) >= 0 
          and (extract(epoch from p.thoigianbd) * 1000 + p.thoigiandau * 60 * 1000 - extract(epoch from now()) * 1000) > 0
          `);
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }

  async endAll() {
    const client = new Client(connection);
    console.log("Class EndAll: get");
    await client.connect();
    try {
      const res = await client.query(
        `update phiendaugia set tinhtrangphiendg = 2 where tinhtrangphiendg = 1 and (extract(epoch from thoigianbd) * 1000 + thoigiandau * 60 * 1000 - extract(epoch from now()) * 1000) < 0 returning *` 
      );
      let phieudaugia = new PhieuDauGia();
      res.rows.forEach(e => {
        phieudaugia.updatetrangthaiphieudgthua(e.maphiendg);
        phieudaugia.updatetrangthaiphieudgthang(e.maphiendg);
      });
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }
};
