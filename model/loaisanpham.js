var connection = require("./connectionString");
const Client = require("pg").Client;

module.exports = class Loaisp {
  constructor(maloaisp = null, tenloaisp = null) {
    this.maloaisp = maloaisp;
    this.tenloaisp = tenloaisp;
  }
  async create(loaisp) {
    const client = new Client(connection);
    console.log("Class loaisp: create");
    await client.connect();
    try {
      const res = await client.query(
        "insert into loaisanpham(tenloaisp) values($1) returning *",
        [loaisp.tenloaisp]
      );
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }
  async read() {
    const client = new Client(connection);
    console.log("Class loaisp: read");
    await client.connect();
    try {
      const res = await client.query("SELECT * from loaisanpham order by maloaisp asc");
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }

  async update(loaisp) {
    const client = new Client(connection);
    console.log("Class loaisp: update");
    await client.connect();
    try {
      const res = await client.query(
        "update loaisanpham set tenloaisp = $1 where maloaisp = $2 returning *",
        [loaisp.tenloaisp, loaisp.maloaisp]
      );
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }

  async delete(id) {
    const client = new Client(connection);
    console.log("Class loaisp: delete");
    await client.connect();
    try {
      const res = await client.query(
        "delete from loaisanpham where maloaisp = $1 returning *",
        [id]
      );
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }

  async get(id) {
    const client = new Client(connection);
    console.log("Class loaisp: get");
    await client.connect();
    try {
      const res = await client.query("select * from loaisanpham where maloaisp = $1", [id]);
      return res.rows;
    } catch (err) {
      console.log(err.stack);
    }
    client.end();
  }
};
