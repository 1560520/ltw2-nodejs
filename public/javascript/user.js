

//set time out
// Set the date we're counting down to
let time = new Date();
let ngay = time.getDate();
let months = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"]
let thang = months[time.getMonth()];
let nam = time.getFullYear();
let gio = time.getHours();
let thamso = 1;
let phut = time.getMinutes() + thamso;
let giay = time.getSeconds();
// var countDownDate = new Date(`Sep 5, 2018 ${gio}:${phut + 31}:${giay}`).getTime();
var countDownDate = new Date(`${thang} ${ngay}, ${nam} ${gio}:${phut}:${giay}`).getTime();

// Update the count down every 1 second
var x = setInterval(function () {

    // Get todays date and time
    var now = new Date().getTime();

    // Find the distance between now an the count down date
    var distance = countDownDate - now;

    // Time calculations for days, hours, minutes and seconds
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    if (hours < 10) {
        hours = "0" + hours;
    }
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    if (seconds < 10) {
        seconds = "0" + seconds;
    }

    // Output the result in an element with id="timeout"
    $("#timeout").html(hours + ":" + minutes + ":" + seconds);

    // If the count down is over, write some text 
    if (distance < 0) {
        clearInterval(x);
        $("#timeout").html("KẾT THÚC");
    }
}, 1000);