const connection = new WebSocket("ws://localhost:12345")

connection.addEventListener("open", () => {
  console.log("Connecting to Server");
});

connection.addEventListener("message", e => {
  if (e.data == maphieudgSelected) {
    clearAllInterval();
    mainChiTietSP();
  }
});

function sendToServer(data) {
  if (connection.readyState === WebSocket.OPEN) {
    connection.send(data);
  } else {
    throw "Not connected";
  }
}

