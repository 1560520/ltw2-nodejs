var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var cookieSession = require("cookie-session");

var pg = require("pg");
pg.defaults.poolSize = 300;
pg.defaults.keepAlive = true;

var mainRouter = require("./controller/main");
var userRouter = require("./controller/user/user");
var taikhoanRouter = require("./controller/common/taikhoan");
var thamsoRouter = require("./controller/common/thamso");
var hinhRouter = require("./controller/common/hinh");
var sanphamRouter = require("./controller/common/sanpham");
var tinhtrangphiendaugiaRouter = require("./controller/common/tinhtrangphiendaugia");
var tinhtrangphieudaugiaRouter = require("./controller/common/tinhtrangphieudaugia");
var phiendaugiaRouter = require("./controller/common/phiendaugia");
var phieudaugiaRouter = require("./controller/common/phieudaugia");
var loaisanphamRouter = require("./controller/common/loaisanpham");
var loaitaikhoanRouter = require("./controller/common/loaitaikhoan");

var PhienDauGia = require("./model/phiendaugia");

let phiendaugia = new PhienDauGia();

var app = express();
app.use(express.static(path.join(__dirname, "public")));
app.set("trust proxy", 1); // trust first proxy

app.use(
  cookieSession({
    name: "session",
    keys: ["key1", "key2"],
    maxAge: 0 * 60 * 1000,
  })
);
app.use((req, res, next) => {
  // req.sessionOptions.maxAge = req.session.maxAge || req.sessionOptions.maxAge;
  phiendaugia.endAll();
  next();
});
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.all("*", (req, res, next) => {
  if (req.xhr || req.headers.accept.indexOf("json") > -1) {
    if (
      (req.session.matk == null || typeof req.session.matk == "undefined") &&
      req.url != "/main" &&
      req.url != "/api/taikhoan/dangnhap" &&
      req.url != "/api/taikhoan/create"
    ) {
      res.statusCode = 403;
      res.end("<h1>Not permission!</h1>");
    } else {
      next();
    }
  } else {
    next(createError(404));
    res.end();
  }
});

app.use("/", mainRouter);
app.use("/user", userRouter);
app.use("/api/taikhoan", taikhoanRouter);
app.use("/api/thamso", thamsoRouter);
app.use("/api/tinhtrangphiendaugia", tinhtrangphiendaugiaRouter);
app.use("/api/tinhtrangphieudaugia", tinhtrangphieudaugiaRouter);
app.use("/api/phieudaugia", phieudaugiaRouter);
app.use("/api/phiendaugia", phiendaugiaRouter);
app.use("/api/loaitaikhoan", loaitaikhoanRouter);
app.use("/api/loaisanpham", loaisanphamRouter);
app.use("/api/sanpham", sanphamRouter);
app.use("/api/hinh", hinhRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
  res.end();
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};
  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

const WebSocket = require("ws"),
  server = new WebSocket.Server({
    port: 12345,
  });

function broadcast(data) {
  server.clients.forEach(ws => {
    ws.send(data);
  });
}

server.on("connection", ws => {
  ws.on("message", data => {
    broadcast(data);
  });
});

module.exports = app;
