var SanPham = require("../../model/sanpham");
var express = require("express");
var formidable = require("formidable");
const fs = require("fs");

var app = express();

const sanpham = new SanPham();

app.get("/read", (req, res, next) => {
  const read = sanpham.read();
  read.then(rs => {
    res.status = 200;
    res.setHeader("Content-Type", "application/json");
    res.send(rs);
    res.end();
  });
  read.catch(err => {
    res.status = 500;
    console.log(err);
  });
});

app.post("/create", (req, res, next) => {
  let form = new formidable.IncomingForm();
  form.parse(req, function (err, fields, files) {
    if (Object.entries(files).length > 0) {
      let oldpath = files.file.path;
      let newpath = "./public/image/" + files.file.name;
      fs.rename(oldpath, newpath, function (err) {
        if (err) throw err;
      });
    }
    let data = fields;
    const create = sanpham.create(
      new SanPham(
        data.masp,
        data.tensp,
        data.maloaisp,
        data.dacta,
        data.hinhdaidien,
        data.isnew,
        data.isdelete,
        data.created,
        data.updated
      )
    );
    create.then(rs => {
      res.status = 200;
      res.setHeader("Content-Type", "application/json");
      res.send(rs);
      res.end();
    });
    create.catch(err => {
      res.status = 500;
      console.log(err);
    });
  });
});

app.put("/update", (req, res, next) => {
  let form = new formidable.IncomingForm();
  form.parse(req, function (err, fields, files) {
    if (Object.entries(files).length > 0) {
      let oldpath = files.file.path;
      let newpath = "./public/image/" + files.file.name;
      fs.rename(oldpath, newpath, function (err) {
        if (err) throw err;
      });
    }
    let data = fields;
    const update = sanpham.update(
      new SanPham(
        data.masp,
        data.tensp,
        data.maloaisp,
        data.dacta,
        data.hinhdaidien,
        data.isnew,
        data.isdelete,
        data.created,
        data.updated
      )
    );
    update.then(rs => {
      res.status = 200;
      res.setHeader("Content-Type", "application/json");
      res.send(rs);
      res.end();
    });
    update.catch(err => {
      res.status = 500;
      console.log(err);
    });
  });
});

app.delete("/delete", (req, res, next) => {
  let data = req.body;
  const remove = sanpham.delete(data.masp);

  remove.then(rs => {
    res.status = 200;
    res.setHeader("Content-Type", "application/json");
    res.send(rs);
    res.end();
  });
  remove.catch(err => {
    res.status = 500;
    console.log(err);
  });
});

app.get("/get/:id", (req, res, next) => {
  let data = req.params;
  const get = sanpham.get(data.id);
  get.then(rs => {
    res.status = 200;
    res.setHeader("Content-Type", "application/json");
    res.send(rs[0]);
    res.end();
  });
  get.catch(err => {
    res.status = 500;
    console.log(err);
  });
});

module.exports = app;
