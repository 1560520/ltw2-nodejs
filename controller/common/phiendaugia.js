var PhienDauGia = require("../../model/phiendaugia");
var express = require("express");

var app = express();

const phiendaugia = new PhienDauGia();

app.get("/read", (req, res, next) => {
  const read = phiendaugia.read();
  read.then(rs => {
    res.status = 200;
    res.setHeader("Content-Type", "application/json");
    res.send(rs);
    res.end();
  });
  read.catch(err => {
    res.status = 500;
    console.log(err);
  });
});

app.post("/create", (req, res, next) => {
  let data = req.body;
  const create = phiendaugia.create(
    new PhienDauGia(
      null,
      data.masp,
      data.thoigianbd,
      data.thoigiandau,
      data.giathapnhat,
      data.giahientai,
      data.maphieudg,
      data.tinhtrangphiendg,
      data.isdelete,
      data.isnew,
      data.updated,
      data.created
    )
  );
  create.then(rs => {
    res.status = 200;
    res.setHeader("Content-Type", "application/json");
    res.send(rs);
    res.end();
  });
  create.catch(err => {
    res.status = 500;
    console.log(err);
  });
});

app.put("/update", (req, res, next) => {
  let data = req.body;
  const update = phiendaugia.update(
    new PhienDauGia(
      data.maphiendg,
      data.masp,
      data.thoigianbd,
      data.thoigiandau,
      data.giathapnhat,
      data.giahientai,
      data.maphieudg,
      data.tinhtrangphiendg,
      data.isdelete,
      data.isnew,
      data.updated,
      data.created
    )
  );
  update.then(rs => {
    res.status = 200;
    res.setHeader("Content-Type", "application/json");
    res.send(rs);
    res.end();
  });
  update.catch(err => {
    res.status = 500;
    console.log(err);
  });
});

app.delete("/delete", (req, res, next) => {
  let data = req.body;
  const remove = phiendaugia.delete(data.maphiendg);
  remove.then(rs => {
    res.status = 200;
    res.setHeader("Content-Type", "application/json");
    res.send(rs);
    res.end();
  });
  remove.catch(err => {
    res.status = 500;
    console.log(err);
  });
});

app.get("/get/:id", (req, res, next) => {
  let data = req.params;
  const get = phiendaugia.get(data.id);
  get.then(rs => {
    res.status = 200;
    res.setHeader("Content-Type", "application/json");
    res.send(rs[0]);
    res.end();
  });
  get.catch(err => {
    res.status = 500;
    console.log(err);
  });
});

app.get("/readwinner/:maphiendg", (req, res, next) => {
  let maphiendg = req.params['maphiendg'];
  const read = phiendaugia.readwinner(maphiendg);
  read.then(rs => {
    res.status = 200;
    res.setHeader("Content-Type", "application/json");
    res.send(rs);
    res.end();
  });
  read.catch(err => {
    res.status = 500;
    console.log("err");
  });
});

app.put("/updatetrangthaiphiendg/:maphiendg", (req, res, next) => {
  let maphiendg = req.params['maphiendg'];
  const update = phiendaugia.updatetrangthaiphiendg(maphiendg);
  update.then(rs => {
    res.status = 200;
    res.setHeader("Content-Type", "application/json");
    res.send(rs);
    res.end();
  });
  update.catch(err => {
    res.status = 500;
    console.log("err");
  });
});

app.get("/readthongtinphiendaugia/:maphiendg", (req, res, next) => {
  let maphiendg = req.params['maphiendg'];
  const read = phiendaugia.readthongtinphiendaugia(maphiendg);
  read.then(rs => {
    res.status = 200;
    res.setHeader("Content-Type", "application/json");
    res.send(rs);
    res.end();
  });
  read.catch(err => {
    res.status = 500;
    console.log("err");
  });
});

app.get("/hometopnew", (req, res, next) => {
  const get = phiendaugia.hometopnew();
  get.then(rs => {
    res.status = 200;
    res.setHeader("Content-Type", "application/json");
    res.send(rs);
    res.end();
  });
  get.catch(err => {
    res.status = 500;
    console.log(err);
  });
});

module.exports = app;
