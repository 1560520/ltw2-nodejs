var LoaiTaiKhoan = require("../../model/loaitaikhoan");
var express = require("express");

var app = express();

const loaitaikhoan = new LoaiTaiKhoan();

app.get("/read", (req, res, next) => {
  const read = loaitaikhoan.read();
  read.then(rs => {
    res.status = 200;
    res.setHeader("Content-Type", "application/json");
    res.send(rs);
    res.end();
  });
  read.catch(err => {
    res.status = 500;
    console.log(err);
  });
});

app.post("/create", (req, res, next) => {
  console.log(req.body);
  let data = req.body;
  const create = loaitaikhoan.create(new LoaiTaiKhoan(null, data.tenloaitk));
  create.then(rs => {
    res.status = 200;
    res.setHeader("Content-Type", "application/json");
    res.send(rs);
    res.end();
  });
  create.catch(err => {
    res.status = 500;
    console.log(err);
  });
});

app.put("/update", (req, res, next) => {
  let data = req.body;
  const update = loaitaikhoan.update(
    new LoaiTaiKhoan(data.maloaitk, data.tenloaitk)
  );
  update.then(rs => {
    res.status = 200;
    res.setHeader("Content-Type", "application/json");
    res.send(rs);
    res.end();
  });
  update.catch(err => {
    res.status = 500;
    console.log(err);
  });
});

app.delete("/delete", (req, res, next) => {
  let data = req.body;
  const remove = loaitaikhoan.delete(data.id);
  remove.then(rs => {
    res.status = 200;
    res.setHeader("Content-Type", "application/json");
    res.send(rs);
    res.end();
  });
  remove.catch(err => {
    res.status = 500;
    console.log(err);
  });
});

app.get("/get/:id", (req, res, next) => {
  let data = req.params;
  const get = loaitaikhoan.get(data.id);
  get.then(rs => {
    res.status = 200;
    res.setHeader("Content-Type", "application/json");
    res.send(rs[0]);
    res.end();
  });
  get.catch(err => {
    res.status = 500;
    console.log(err);
  });
});

module.exports = app;
