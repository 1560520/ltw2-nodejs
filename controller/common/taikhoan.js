var TaiKhoan = require("../../model/taikhoan");
var express = require("express");

var app = express();

const taikhoan = new TaiKhoan();

app.get("/read", (req, res, next) => {
  const read = taikhoan.read();
  read.then(rs => {
    res.status = 200;
    res.setHeader("Content-Type", "application/json");
    res.send(rs);
    res.end();
  });
  read.catch(err => {
    res.status = 500;
    console.log(err);
  });
});

app.post("/create", (req, res, next) => {
  let data = req.body;
  const create = taikhoan.create(
    new TaiKhoan(
      null,
      data.tendangnhap,
      data.matkhau,
      data.tenhienthi,
      data.email,
      data.dienthoai,
      data.diachi,
      data.maloaitk
    )
  );
  create.then(rs => {
    res.status = 200;
    res.setHeader("Content-Type", "application/json");
    res.send(rs);
    res.end();
  });
  create.catch(err => {
    res.status = 500;
    console.log(err);
  });
});

app.put("/update", (req, res, next) => {
  let data = req.body;
  const update = taikhoan.update(
    new TaiKhoan(
      data.matk,
      data.tendangnhap,
      data.matkhau,
      data.tenhienthi,
      data.email,
      data.dienthoai,
      data.diachi,
      data.maloaitk
    )
  );
  update.then(rs => {
    res.status = 200;
    res.setHeader("Content-Type", "application/json");
    res.send(rs);
    res.end();
  });
  update.catch(err => {
    res.status = 500;
    console.log(err);
  });
});

app.delete("/delete", (req, res, next) => {
  let data = req.body;
  const remove = taikhoan.delete(matk);
  remove.then(rs => {
    res.status = 200;
    res.setHeader("Content-Type", "application/json");
    res.send(rs);
    res.end();
  });
  remove.catch(err => {
    res.status = 500;
    console.log(err);
  });
});

app.post("/dangnhap", (req, res, next) => {
  let data = req.body;
  const get = taikhoan.get(data.tendangnhap, data.matkhau);
  get.then(rs => {
    if (rs.length > 0) {
      res.status = 200;
      res.setHeader("Content-Type", "application/json");
      req.session.matk = rs[0].matk;
      req.session.maloaitk = rs[0].maloaitk;
      res.redirect("/main");
    } else {
      res.statusCode = 403;
      res.setHeader("Content-Type", "application/json");
      res.send({
        error: true,
        description: "username or password are not correct, or your account is banned",
      });
    }
    res.end();
  });
  get.catch(err => {
    res.status = 500;
    console.log(err);
  });
});

app.get("/dangxuat", (req, res, next) => {
  res.status = 200;
  res.setHeader("Content-Type", "text/html");
  req.session = null;
  res.redirect("/main");
  res.end();
});

app.get("/getuser", (req, res, next) => {
  const getUser = taikhoan.getUser(req.session.matk);
  getUser.then(rs => {
    res.status = 200;
    res.setHeader("Content-Type", "application/json");
    res.send(rs);
    res.end();
  });
  getUser.catch(err => {
    res.status = 500;
    console.log(err);
  });
});

module.exports = app;
