var TinhTrangPhieuDauGia = require("../../model/tinhtrangphieudaugia");
var express = require("express");

var app = express();

const tinhtrangphieudaugia = new TinhTrangPhieuDauGia();

app.get("/read", (req, res, next) => {
    const read = tinhtrangphieudaugia.read();
    read.then(rs => {
        res.status = 200;
        res.setHeader("Content-Type", "application/json");
        res.send(rs);
        res.end();
    });
    read.catch(err => {
        res.status = 500;
        console.log(err);
    });
});

app.post("/create", (req, res, next) => {
    let data = req.body;
    const create = tinhtrangphieudaugia.create(
        new TinhTrangPhieuDauGia(null, data.tentinhtrangphieudg)
    );
    create.then(rs => {
        res.status = 200;
        res.setHeader("Content-Type", "application/json");
        res.send(rs);
        res.end();
    });
    create.catch(err => {
        res.status = 500;
        console.log(err);
    });
});

app.put("/update", (req, res, next) => {
    let data = req.body;
    const update = tinhtrangphieudaugia.update(
        new TinhTrangPhieuDauGia(
            data.matinhtrangphieudg,
            data.tentinhtrangphieudg
        )
    );
    update.then(rs => {
        res.status = 200;
        res.setHeader("Content-Type", "application/json");
        res.send(rs);
        res.end();
    });
    update.catch(err => {
        res.status = 500;
        console.log(err);
    });
});

app.delete("/delete", (req, res, next) => {
    let data = req.body;
    const remove = tinhtrangphieudaugia.delete(data.id);
    remove.then(rs => {
        res.status = 200;
        res.setHeader("Content-Type", "application/json");
        res.send(rs);
        res.end();
    });
    remove.catch(err => {
        res.status = 500;
        console.log(err);
    });
});

app.get("/get/:id", (req, res, next) => {
    let data = req.params;
    const get = tinhtrangphieudaugia.get(data.id);
    get.then(rs => {
        res.status = 200;
        res.setHeader("Content-Type", "application/json");
        res.send(rs[0]);
        res.end();
    });
    get.catch(err => {
        res.status = 500;
        console.log(err);
    });
});

module.exports = app;