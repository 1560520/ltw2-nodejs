var PhieuDauGia = require("../../model/phieudaugia");
var express = require("express");
var app = express();
const phieudaugia = new PhieuDauGia();

app.get("/read", (req, res, next) => {
    const read = phieudaugia.read();
    read.then(rs => {
        res.status = 200;
        res.setHeader("Content-Type", "application/json");
        res.send(rs);
        res.end();
    });
    read.catch(err => {
        res.status = 500;
        console.log(err);
    });
});

app.get("/readGioHang", (req, res, next) => {
    const read = phieudaugia.readGioHang(req.session.matk);
    read.then(rs => {
        res.status = 200;
        res.setHeader("Content-Type", "application/json");
        res.send(rs);
        res.end();
    });
    read.catch(err => {
        res.status = 500;
        console.log(err);
    });
});

app.post("/create", (req, res, next) => {
    let data = req.body;
    const create = phieudaugia.create(
        new PhieuDauGia(
            null,
            data.maphiendg,
            data.matk,
            data.giadau,
            data.tinhtrangphieudg,
            data.thoigiandau
        )
    );
    create.then(rs => {
        res.status = 200;
        res.setHeader("Content-Type", "application/json");
        res.send(rs);
        res.end();
    });
    create.catch(err => {
        res.status = 500;
        console.log(err);
    });
});

app.put("/update", (req, res, next) => {
    let data = req.body;
    const update = phieudaugia.update(
        new PhieuDauGia(
            data.maphieudg,
            data.maphiendg,
            data.matk,
            data.giadau,
            data.tinhtrangphieudg,
            data.thoigiandau
        )
    );
    update.then(rs => {
        res.status = 200;
        res.setHeader("Content-Type", "application/json");
        res.send(rs);
        res.end();
    });
    update.catch(err => {
        res.status = 500;
        console.log(err);
    });
});

app.delete("/delete", (req, res, next) => {
    let data = req.body;
    const remove = phieudaugia.delete(data.id);
    remove.then(rs => {
        res.status = 200;
        res.setHeader("Content-Type", "application/json");
        res.send(rs);
        res.end();
    });
    remove.catch(err => {
        res.status = 500;
        console.log(err);
    });
});

app.get("/get/:id", (req, res, next) => {
    let data = req.params;
    const get = phieudaugia.get(data.id);
    get.then(rs => {
        res.status = 200;
        res.setHeader("Content-Type", "application/json");
        res.send(rs[0]);
        res.end();
    });
    get.catch(err => {
        res.status = 500;
        console.log(err);
    });
});

app.put("/uploadGioHang", (req, res, next) => {
    const update = phieudaugia.uploadGioHang();
    update.then(rs => {
        res.status = 200;
        res.setHeader("Content-Type", "application/json");
        res.send(rs);
        res.end();
    });
    update.catch(err => {
        res.status = 500;
        console.log(err);
    });
});

app.put("/xoaGioHang", (req, res, next) => {
    let data = req.body;
    const update = phieudaugia.xoaGioHang(
        data.maphieudg
    );
    update.then(rs => {
        res.status = 200;
        res.setHeader("Content-Type", "application/json");
        res.send(rs);
        res.end();
    });
    update.catch(err => {
        res.status = 500;
        console.log(err);
    });
});

app.put("/updatetrangthaiphieudgthua/:maphiendg", (req, res, next) => {
    let maphiendg = req.params['maphiendg'];
    const update = phieudaugia.updatetrangthaiphieudgthua(maphiendg);
    update.then(rs => {
        res.status = 200;
        res.setHeader("Content-Type", "application/json");
        res.send(rs);
        res.end();
    });
    update.catch(err => {
        res.status = 500;
        console.log("err");
    });
});

app.put("/updatetrangthaiphieudgthang/:maphiendg", (req, res, next) => {
    let maphiendg = req.params['maphiendg'];
    const update = phieudaugia.updatetrangthaiphieudgthang(maphiendg);
    update.then(rs => {
        res.status = 200;
        res.setHeader("Content-Type", "application/json");
        res.send(rs);
        res.end();
    });
    update.catch(err => {
        res.status = 500;
        console.log("err");
    });
});

app.get("/readdaugiacuatoithang", (req, res, next) => {
    const read = phieudaugia.readdaugiacuatoithang(req.session.matk);
    read.then(rs => {
        res.status = 200;
        res.setHeader("Content-Type", "application/json");
        res.send(rs);
        res.end();
    });
    read.catch(err => {
        res.status = 500;
        console.log(err);
    });
});

app.get("/readdaugiacuatoithua", (req, res, next) => {
    const read = phieudaugia.readdaugiacuatoithua(req.session.matk);
    read.then(rs => {
        res.status = 200;
        res.setHeader("Content-Type", "application/json");
        res.send(rs);
        res.end();
    });
    read.catch(err => {
        res.status = 500;
        console.log(err);
    });
});

app.get("/readdaugiacuatoidangdau", (req, res, next) => {
    const read = phieudaugia.readdaugiacuatoidangdau(req.session.matk);
    read.then(rs => {
        res.status = 200;
        res.setHeader("Content-Type", "application/json");
        res.send(rs);
        res.end();
    });
    read.catch(err => {
        res.status = 500;
        console.log(err);
    });
});

module.exports = app;