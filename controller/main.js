var express = require("express");
const fs = require("fs");
var app = express();

app.get("/main", (req, res, next) => {
  if (req.session.matk == null || req.session.matk == undefined) {
    fs.readFile("./public/common/login.html", "utf-8", (err, data) => {
      if (err) {
        res.status = 500;
        res.send("_Error");
      } else {
        res.statusCode = 200;
        res.setHeader("Content-Type", "text/html");
        res.send(data);
        res.end();
      }
    });
  } else if (req.session.maloaitk == 1) {
    fs.readFile("./public/admin/admin.html", "utf-8", (err, data) => {
      if (err) {
        res.status = 500;
        res.send("_Error");
      } else {
        res.statusCode = 200;
        res.setHeader("Content-Type", "text/html");
        res.send(data);
        res.end();
      }
    });
  }else{
    fs.readFile("./public/user/user.html", "utf-8", (err, data) => {
      if (err) {
        res.status = 500;
        res.send("_Error");
      } else {
        res.statusCode = 200;
        res.setHeader("Content-Type", "text/html");
        res.send(data);
        res.end();
      }
    });
  }
});



module.exports = app;
