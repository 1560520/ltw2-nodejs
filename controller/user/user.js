var express = require("express");
var fs = require("fs");
var app = express();

app.get("/main", (req, res, next) => {
  fs.readFile("./public/user/main.html", "utf-8", (err, data) => {
    if (err) {
      res.status = 500;
      res.send("_Error");
    } else {
      res.statusCode = 200;
      res.setHeader("Content-Type", "text/html");
      res.send(data);
      res.end();
    }
  });
});

app.get("/daugiacuatoi", (req, res, next) => {
  fs.readFile("./public/user/daugiacuatoi.html", "utf-8", (err, data) => {
    if (err) {
      res.status = 500;
      res.send("_Error");
    } else {
      res.statusCode = 200;
      res.setHeader("Content-Type", "text/html");
      res.send(data);
      res.end();
    }
  });
});

app.get("/giohang", (req, res, next) => {
  fs.readFile("./public/user/giohang.html", "utf-8", (err, data) => {
    if (err) {
      res.status = 500;
      res.send("_Error");
    } else {
      res.statusCode = 200;
      res.setHeader("Content-Type", "text/html");
      res.send(data);
      res.end();
    }
  });
});

app.get("/thongtincanhan", (req, res, next) => {
  fs.readFile("./public/user/thongtincanhan.html", "utf-8", (err, data) => {
    if (err) {
      res.status = 500;
      res.send("_Error");
    } else {
      res.statusCode = 200;
      res.setHeader("Content-Type", "text/html");
      res.send(data);
      res.end();
    }
  });
});

app.get("/chitietsanpham", (req, res, next) => {
  fs.readFile("./public/user/chitietsanpham.html", "utf-8", (err, data) => {
    if (err) {
      res.status = 500;
      res.send("_Error");
    } else {
      res.statusCode = 200;
      res.setHeader("Content-Type", "text/html");
      res.send(data);
      res.end();
    }
  });
});

module.exports = app;
